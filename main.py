import requests

# GETTING RIGHT CRYPTOCURRENCY NAME

not_crypto = True
while not_crypto:
    try:
        STOCK = input('What cryptocurrency are you interested in? (use abbreviation)\n')
        STOCK_ENDPOINT = "https://www.alphavantage.co/query"
        NEWS_ENDPOINT = "https://newsapi.org/v2/everything"

        stock_params = {
            'function': 'DIGITAL_CURRENCY_DAILY',
            'symbol': STOCK,
            'market': 'CNY',
            # USE YOUR ALPHAVANTAGE APIKEY HERE, GET IT FROM https://www.alphavantage.co
            'apikey': 'YOUR APIKEY',
        }

        stock_request = requests.get(STOCK_ENDPOINT, params=stock_params)
        stock_request.raise_for_status()
        data = stock_request.json()
        daily_data = data['Time Series (Digital Currency Daily)']
        data_days_list = [[key, value] for key, value in daily_data.items()]
        not_crypto = False
    except KeyError:
        print(f'{STOCK} not found')

# GETTING PRICE CHANGE OF CURRENCY IN ONE DAY

last_days = data_days_list[:2]
yesterday = last_days[0]
previous_day = last_days[1]
yesterday_price = float(yesterday[1]['4b. close (USD)'])
previous_day_price = float(previous_day[1]['4b. close (USD)'])
price_change = ''
diff_price = ''
if yesterday_price > previous_day_price:
    diff_price = round(yesterday_price - previous_day_price, 0)
    price = round((yesterday_price/previous_day_price*100) - 100, 1)
    price_change = f'{STOCK.upper()}: {yesterday_price}$, 🔺{price}%({diff_price})$\n'
else:
    diff_price = round(previous_day_price - yesterday_price, 0)
    price = round((previous_day_price/yesterday_price*100) - 100, 1)
    price_change = f'{STOCK}: {yesterday_price}$, 🔻{price}% (-{diff_price})$\n'

print(price_change)

# GETTING LATEST NEWS

news_params = {
    # USE YOUR NEWSAPI APIKEY HERE, GET IT FROM https://newsapi.org/
    'apiKey': 'YOUR APIKEY',
    'qInTitle': STOCK.upper(),
}

news_request = requests.get(NEWS_ENDPOINT, params=news_params)
news_request.raise_for_status()
news_data = news_request.json()['articles']
first_five_articles = news_data[:5]
for article in first_five_articles:
    description = f"Headline: {article['title']}.\nBrief: {article['description']}.\nLink: {article['url']}\n"
    print(description)
